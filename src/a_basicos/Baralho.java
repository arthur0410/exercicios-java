package a_basicos;

/**
 * Crie um programa que imprima todas as cartas do baralho.
 * 
 * Exemplo: 
 * 
 * Ás de Ouros
 * Ás de Espadas
 * Ás de Copas
 * Ás de Paus
 * Dois de Ouros
 * ...
 * 
 */
public class Baralho {
	
	public static void main (String args[]) {
		String naipes[] = {"Ouros", "Espadas", "Copas", "Paus"};
		String cartas[] = {"Ás", "Dois", "Três", "Quatro", "Cinco", "Seis", "Sete", 
							"Oito", "Nove", "Valete", "Dama", "Rei"};
		
		
		for(String carta : cartas) {
			for(String naipe : naipes) {
				System.out.println(carta + " de " + naipe);
			}
		}
	}

}