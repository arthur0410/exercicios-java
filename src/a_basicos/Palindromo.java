package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe uma palavra do terminal e determina
 * se ela é um palíndromo.
 * 
 * Exs: 
 * 
 * input: ovo
 * output: A palavra ovo é um palíndromo
 * 
 * input: jose
 * output: A palavra jose não é um palíndromo 
 */
public class Palindromo {
	public static void main(String args[]) {
		
		Scanner scanner = new Scanner(System.in);
		
		boolean ehPalindromo = true;
		
		System.out.println("Digite a palavra:");
		String palavra = scanner.nextLine();
		
		for (int i = 0; i < palavra.length()/2; i++) {
			
			char inicio = palavra.charAt(i);
			char fim = palavra.charAt(palavra.length() - i - 1);
			
			if(inicio != fim) {	
				ehPalindromo = false;
			}
			
			if (ehPalindromo){
				break;
			}
			
		}
		
		if(ehPalindromo) {
			System.out.println("É palindromo.");
		}
		else {
			System.out.println("Não é palindromo.");
		}
		
	}
}