package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe as três dimensões de uma piscina
 * (largura, comprimento, profundidade) e retorne a sua capacidade
 * em litros.
 */
public class Piscina {
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite a largura, comprimento e profundidade:");
		int largura = scanner.nextInt();
		int comprimento = scanner.nextInt();
		int profundidade = scanner.nextInt();
		
		int volume = largura * comprimento * profundidade;
		int volumeEmLitros = volume * 1000;
		
		System.out.println("A capacidade da piscina é de " + volumeEmLitros + " litros.");
		
		scanner.close();
		
	}
}