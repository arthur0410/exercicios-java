package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe um número do usuário
 * e imprime a fatorial desse mesmo número.
 * 
 */
public class Fatorial {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite um número inteiro:");
		int numero = scanner.nextInt();
		
		int resultado = 1;
		
		for (int i = numero; i > 0; i--) {
			resultado = resultado * i;
		}
		
		System.out.println("O fatorial deste número é " + resultado);
	}
}