package a_basicos;

public class Primo {
	/**
	 * Crie um programa que imprima todos os números
	 * primos de 1 à 100
	 */
	public static void main(String[] args) {
		
		int quantidadeDivisores = 0;
		
		for (int numero = 1; numero <= 100 ; numero++) {
			
			quantidadeDivisores = 0;
			
			for (int i = 1; i < numero; i++) {
				if(numero%i == 0) {
					quantidadeDivisores++;
				}
			}
			
			if (quantidadeDivisores == 1) {
				System.out.println(numero + " ");
			}
		}
		
	}
}