package b_intermediarios;

import java.util.ArrayList;

public class CacaNiquelClass {
	
	String[] sorteados;
	String[] valores = {"abacaxi", "framboesa", "banana", "pera", "7", "Maçã"};
	int resultado;
	
	public CacaNiquelClass() {
		sorteados = new String[3];
		resultado = 0;
	}

	public void sortear() {
		
		int valorAleatorio;
		double numero;
		
		for (int i = 0; i < 3; i++) {
			numero = Math.random() * 4;
			valorAleatorio = (int) (Math.round(numero));
			sorteados[i] = valores[valorAleatorio];
		}
		
		calcularResultado();
	}
	
	public String[] mostrarSorteio(){
		
		return sorteados;
		
	}
	
	private void calcularResultado() {
		
		if(sorteados[0] == "7" && sorteados[1] == "7" && sorteados[2] == "7") {
			resultado = 5000;
		}
		
		int quantidadePontos = 0;
		
		int quantidadeIguais = calcularQuantidadeIguais(sorteados);
		
        if (quantidadeIguais == 2) {
        	quantidadePontos = 100;
        } else {
        	if (quantidadeIguais == 6) {
        		quantidadePontos = 1000;
        	}
        }
        
		resultado = quantidadePontos;
	}
	
	public static int calcularQuantidadeIguais(String[] resultado) {
		ArrayList<String> lista = new ArrayList<String>();
		int quantidadeIguais = 0;
		
        for (int i = 0; i < resultado.length; i++) {

            for (int j = 0; j < resultado.length; j++) {
                   
                  if(resultado[i]==resultado[j]&& j!=i){
                         if(!lista.contains(resultado[i]))
                         {
                                quantidadeIguais++;
                         }
                  }
            }       
        }
        
        return quantidadeIguais;
	}
	
}
