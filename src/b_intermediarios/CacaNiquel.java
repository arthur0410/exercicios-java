package b_intermediarios;

import java.util.ArrayList;

/**
 * Crie um programa que simula o funcionamento de um caça níquel, da seguinte forma:
 * 
 * 1- Sorteie 3x um valor dentre as strings do vetor de valores
 * 2- Compare os valores sorteados e determine os pontos obtidos usando o
 * seguinte critério:
 * 
 * Caso hajam três valores diferentes: 0 pontos
 * Caso hajam dois valores iguais: 100 pontos
 * Caso hajam três valores iguais: 1000 pontos
 * Caso hajam três valores "7": 5000 pontos
 * 
 */
public class CacaNiquel {
	
	public static void main(String args[]) {
		
		String[] valores = {"abacaxi", "framboesa", "banana", "pera", "7"};
		
		String[] resultado = sortear(valores);
		
//		forçando tudo igual
//		for (int i = 0; i < resultado.length; i++) {
//			resultado[i] = "banana";
//		}
			
		int quantidadePontos = calcularQuantidadePontos(resultado);
		
		System.out.println("Quantidade de pontos: " + quantidadePontos);
	}
	
	public static String[] sortear(String[] valores) {
		
		double numero;
		int valorAleatorio;
		String[] resultado = new String[3];
		
		for (int i = 0; i < 3; i++) {
			numero = Math.random() * 4;
			valorAleatorio = (int) (Math.round(numero));
			System.out.println(valores[valorAleatorio]);
			resultado[i] = valores[valorAleatorio];
		}
		
		return resultado;
	}
	
	public static int calcularQuantidadePontos(String[] resultado) {
		
		if(resultado[0] == "7" && resultado[1] == "7" && resultado [2] == "7") {
			return 5000;
		}
		
		int quantidadePontos = 0;
		
		int quantidadeIguais = calcularQuantidadeIguais(resultado);
		
        if (quantidadeIguais == 2) {
        	quantidadePontos = 100;
        } else {
        	if (quantidadeIguais == 6) {
        		quantidadePontos = 1000;
        	}
        }
        
		return quantidadePontos;
	}
	
	public static int calcularQuantidadeIguais(String[] resultado) {
		ArrayList<String> lista = new ArrayList<String>();
		int quantidadeIguais = 0;
		
        for (int i = 0; i < resultado.length; i++) {

            for (int j = 0; j < resultado.length; j++) {
                   
                  if(resultado[i]==resultado[j]&& j!=i){
                         if(!lista.contains(resultado[i]))
                         {
                                quantidadeIguais++;
                         }
                  }
            }       
        }
        
        return quantidadeIguais;
	}
	
}