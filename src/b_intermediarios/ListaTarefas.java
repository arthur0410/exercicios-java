package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Crie um programa que recebe um todo list e a escreve em um arquivo txt.
 * 
 * A cada iteração, o sistema deve pedir uma  única tarefa e exibir a possibilidade
 * de encerrar o programa.
 * 
 * Quando o usuário encerrar o programa, deve-se gerar o arquivo txt com as tarefas
 * que foram inseridas.
 *
 */
public class ListaTarefas {
	public static void main(String[] args) {
		
		Path path = Paths.get("teste.txt");
		String entradaDeumaTarefa= "Esse é meu arquivo teste";
		String textoinfinitoFinal =" " ;
		
		Scanner scanner = new Scanner(System.in);
		boolean sairdoPrograma=false;
		
		
			
	
		try {
			
			do { 
				System.out.println("Digite uma tarefa, ou VAZAR para sair");
				entradaDeumaTarefa = scanner.next();
				
				if(entradaDeumaTarefa.equals("VAZAR")) {
					sairdoPrograma=true;
					break;
					}
				else {
					textoinfinitoFinal = textoinfinitoFinal + entradaDeumaTarefa +" ";
				}
				}
				while(sairdoPrograma==false);
			
			Files.write(path, textoinfinitoFinal.getBytes());	
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}

