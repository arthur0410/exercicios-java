package b_intermediarios;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Dado um numero inteiro positivo, encontre 
 * a menor soma de dois números cujo produto
 * seja igual ao número
 * 
 * Ex: se n = 10, então resultado = 7, pois 5 * 2 
 * é o menor produto inteiro positivo que resulta
 * 10.
 * 
 * Testes:
 * 
 */
public class ComplexidadeInteiros {
	public static void main(String[] args) {
		
		// pedir número
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite um numero inteiro positivo:");
		int numero = scanner.nextInt();
		
		// encontrar a soma de todos os produtos que resultam no inteiro
		ArrayList<Integer>listaResultados = encontrarSomas(numero);
		
		// encontrar a menor soma
		int menorSoma = encontrarMenorSoma(listaResultados);
		
		System.out.println("A menor soma é " + menorSoma);
	}
	
	public static ArrayList<Integer> encontrarSomas(int numero) {
		
		ArrayList<Integer> lista = new ArrayList<Integer>();
		
		for (int i = 1; i <= numero; i++) {
			for (int j = 0; j <= numero; j++) {
				if(i*j == numero) {
					System.out.println("i= " + i + " e j= " + j);
					lista.add(i+j);
				}
			}
		}
		
		return lista;
	}
	
	public static int encontrarMenorSoma(ArrayList<Integer> listaResultados) {

		int menorSoma = 999999999;
		
		for (int i = 0; i < listaResultados.size(); i++) {
			if (listaResultados.get(i) < menorSoma) {
				menorSoma = listaResultados.get(i);
			}
		}
		
		return menorSoma;
	}
}