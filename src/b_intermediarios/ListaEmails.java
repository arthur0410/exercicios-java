package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Crie um programa que lê os clientes presentes no arquivo contas.csv e 
 * filtre aqueles que possuem o saldo superior à 7000.
 * Desses clientes, criar outro arquivo de texto que possua todos os clientes
 * no seguinte formato:
 * 
 * João da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 */
public class ListaEmails {
	public static void main(String[] args) {
		
		ArrayList<String> linhas = new ArrayList<>();
		
		Path path = Paths.get("contas.csv");
		Path path2 = Paths.get("emails.txt");
		StringBuilder listaEmails = new StringBuilder();

		try {
			linhas = (ArrayList<String>) Files.readAllLines(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String[] colunas = new String[5];
		
		for(String linha : linhas) {
			colunas = linha.split(",");
			int saldo = 0;
			
			try {
				saldo = Integer.parseInt(colunas[4]);
			} catch(Exception e) {
				
			}
			
			
			if(saldo > 7000) {
				String email = String.format("%s %s<%s>, ", colunas[1], colunas[2], colunas[3]);
				listaEmails.append(email);
			}
		}
		
		try {
			Files.write(path2, listaEmails.toString().getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
