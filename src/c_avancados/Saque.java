package c_avancados;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Crie um programa que faça um saque no arquivo contas.csv. O programa
 * deve receber um id e o valor do saque. Ele deve informar caso o id
 * não seja encontrado ou caso o valor não esteja disponível.
 * 
 * Após o saque, o arquivo deve ser atualizado.
 * 
 */
public class Saque {
	public static void main(String[] args) {
	
		Scanner scanner = new Scanner(System.in);
		
		// perguntar id do usuario	
		System.out.println("Qual o ID do usuario ?");
		int idUsuario = scanner.nextInt();
		
		// perguntar o quanto ele quer sacar
		System.out.println("Qual o valor do saque?");
		int valorSaque = scanner.nextInt();
		
		// ler o arquivo inteiro e guardar na List<String>
		ArrayList<String> arquivoContas = lerContas();
		
		// sacar dinheiro (deverá verificar se possui saldo)
		arquivoContas = sacarDinheiro(idUsuario, valorSaque, arquivoContas);
		
		// atualiza arquivo contas
		gravarArquivo(arquivoContas);
		
	}
	
	public static ArrayList<String> lerContas() {
		
		Path path = Paths.get("contas.csv");
		ArrayList<String> linhas = new ArrayList<>();
		
		try {
			linhas = (ArrayList<String>) Files.readAllLines(path);
			return linhas;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	public static ArrayList<String> sacarDinheiro(int idUsuario, int valorSaque, ArrayList<String> arquivo) {
		
		boolean achouCliente = false;
		
		for (int i = 0; i < arquivo.size(); i++) {
	
			String[] partes = new String[5];
			partes = arquivo.get(i).split(",");
			
			try {
				int idUsuarioArquivo = Integer.parseInt(partes[0]);
				
				if(idUsuarioArquivo == idUsuario) {
					
					achouCliente = true;
						
					 int saldo = 0;
					 
					 try {
							saldo = Integer.parseInt(partes[4]);
							if(saldo >= valorSaque) {
								partes[4] = Integer.toString(saldo - valorSaque);
								String linha = "";
								for (int j = 0; j < partes.length; j++) {
									if(j != 4) {
										linha += partes[j] + ",";
									} else {
										linha += partes[j];
									}
								}
								arquivo.set(i, linha);
							} else {
								System.out.println("Saldo insuficiente");
							}
					 } catch(Exception e) {
					 }

				}
			} catch (Exception e) {

			}
		}
		
		if(!achouCliente) {
			System.out.println("Cliente não encontrado");
		}
		
		return arquivo;
	}
	
	public static void gravarArquivo(ArrayList<String> arquivo) {
		Path path = Paths.get("contas.csv");
		try {
			Files.write(path, arquivo);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
}
